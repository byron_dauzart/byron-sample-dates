﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Samples.FirstProjectSample.Core.Models.Messages
{
    public class GetResponse<T>
    {
        public bool IsSuccessful { get; set; }
        public T Payload { get; set; }
        public Exception Error { get; set; }
        public string Message { get; set; }
    }
}
