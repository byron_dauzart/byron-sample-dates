using System;
using Xunit;
using Samples.FirstProjectSample.Infrastructure.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Samples.FirstProjectSample.Testing
{
    public class MainTests
    {
        [Fact]
        public void Test1()
        {
            var x = 3;
            
            Assert.Equal(27,x);
            Assert.True(x == 27);
            Assert.Equal(100, x);
        }

        [Fact]
        public async Task InsertDate()
        {
            var dlist = new List<DateTime>();
            dlist.Add(DateTime.Now);

            var repo = new DatesRepository();
            var response = await repo.InsertInvalidDates(dlist);
            Assert.True(response.IsSuccessful);
        }

    }
}
