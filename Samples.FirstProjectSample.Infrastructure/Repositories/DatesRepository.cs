﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Samples.FirstProjectSample.Infrastructure.Factories;
using Samples.FirstProjectSample.Core.Models.Messages;
using System.Threading.Tasks;
using Dapper;

namespace Samples.FirstProjectSample.Infrastructure.Repositories
{
    public class DatesRepository
    {
        private readonly ConnectionFactory _connectionFactory;
        public DatesRepository()
        {
            _connectionFactory = new ConnectionFactory();
        }

        public async Task<SaveResponse> InsertInvalidDates(IEnumerable<DateTime> invalidDates)
        {
            var response = new SaveResponse();
            try
            {
                using (var conn = await _connectionFactory.GetSqlConnection())
                {
                    await conn.ExecuteAsync(QueryInsertInvalidDates, ListToDto(invalidDates));
                    response.IsSuccessful = true;
                }
            }
            catch (Exception e)
            {
                response.IsSuccessful = false;
                response.Error = e;
                response.Message = "An error occurred while attempting to insert invalid dates.";
            }

            return response;
        }


        public async Task<GetResponse<IEnumerable<DateTime>>> GetInvalidDates()
        {
            var response = new GetResponse<IEnumerable<DateTime>>();

            try
            {
                using (var conn = await _connectionFactory.GetSqlConnection())
                {
                    response.Payload = await conn.QueryAsync<DateTime>(QueryGetInvalidDates, null);
                    response.IsSuccessful = true;
                }
            }
            catch (Exception e)
            {
                response.IsSuccessful = false;
                response.Payload = null;
                response.Error = e;
                response.Message = "An error occurred while attempting to retrieve invalid dates.";
            }

            return response;
        }

        #region Private Helpers
        private class InvalidDateDto
        {
            public DateTime InvalidDate { get; set; }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Converts list of DateTime values into DTOs for insertion into SQL.
        /// </summary>
        /// <param name="invalidDates"></param>
        /// <returns></returns>
        private IEnumerable<InvalidDateDto> ListToDto(IEnumerable<DateTime> invalidDates)
        {
            return invalidDates.Select(x => new InvalidDateDto() { InvalidDate = x });
        }

        #endregion

        #region SQL Scripts

        private const string QueryGetInvalidDates = @"SELECT * FROM InvalidDates";
        private const string QueryInsertInvalidDates = @"INSERT INTO InvalidDates VALUES (@invalidDate)";

        #endregion
    }
}
