﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Samples.FirstProjectSample.Infrastructure.Factories
{
    public class ConnectionFactory
    {
        private const string ConnectionString = @"server=v-iis01.techneaux.int;database=FirstSampleProject;integrated security=true;trusted_connection=yes;multipleactiveresultsets=true;";
        public async Task<SqlConnection> GetSqlConnection()
        {
            var conn = new SqlConnection(ConnectionString);
            await conn.OpenAsync();
            return conn;
        }
    }
}
