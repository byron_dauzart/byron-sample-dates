﻿using System;
using System.Collections.Generic;
using System.Linq;
using Samples.FirstProjectSample.Infrastructure.Repositories;
using System.Threading.Tasks;
using Samples.FirstProjectSample.Core.Models.Messages;

namespace Samples.FirstProjectSample.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime startDate = new DateTime(2019, 1, 1);
            DateTime endDate = new DateTime(2023, 1, 1);

            beginning:
            Console.WriteLine(@"Which action would you like to take?
1) Sync invalid dates from file.
2) Generate accepted dates from date range.");

            var key = Console.ReadKey();
            if(!int.TryParse(key.KeyChar.ToString(), out var actionId))
            {
                Console.WriteLine(@"Invalid character. Please try again.");
                goto beginning;
            }

            switch (actionId)
            {
                case 1: SyncInvalidDates();
                    break;
                case 2:
                    break;
            }
            
           // List<DateTime> validDates = GenerateAcceptedDates(startDate, endDate);

        
           // foreach (DateTime date in validDates)
           //     Console.WriteLine(date);
           // Console.ReadKey();
        }

        #region Private Methods

        /// <summary>
        /// Start looping from the startDate and go till the endDate (exclusive)
        /// You want to skip over dates that are disqualified
        ///     A date is disqualified if it is a weekend or a holiday for Trinidad
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private static List<DateTime> GenerateAcceptedDates(DateTime startDate, DateTime endDate)
        {
            List<DateTime> allWeekdayDates = new List<DateTime>();
            List<DateTime> invalidDates = GetInvalidDates();
            
            //Fill allWeekDay list with 2019-2023 weekdays
            for (DateTime day = startDate; day.Date < endDate.Date; day = day.AddDays(1))
            {
                if(true == (day.DayOfWeek != DayOfWeek.Saturday) && true == (day.DayOfWeek != DayOfWeek.Sunday))
                {
                    allWeekdayDates.Add(day);
                }
            }
            //Compare weekday List to List of invalid dates provided to get List of valid dates
            IEnumerable<DateTime> validDates = allWeekdayDates.Except(invalidDates);
            List<DateTime> passableValidDates = validDates.ToList<DateTime>();
            
            return passableValidDates;
        }
        /// <summary>
        /// Retrieves invalid dates and sends to be inserted into database
        /// </summary>
        private static void SyncInvalidDates()
        {
            var invalidDates = GetInvalidDates();
            InsertInvalidDates(invalidDates);
        }
        /// <summary>
        /// Retrieves invalid dates from a text file
        /// </summary>
        /// <returns></returns>
        private static List<DateTime> GetInvalidDates()
        {
            string path = @"C:\repos\Samples\First Project Sample\InvalidDates.txt";
            String[] textInvalidDates = System.IO.File.ReadAllLines(path);
            List<DateTime> invalidDates = textInvalidDates.Select(x=> DateTime.Parse(x)).ToList();

            return invalidDates;
        }

        /// <summary>
        /// Utilizes repo to add new invalid dates.
        /// </summary>
        /// <param name="invalidDates"></param>
        private static void InsertInvalidDates(List<DateTime> invalidDates)
        {
            var repo = new DatesRepository();
            var t = Task.Run(async () =>
            {
                return await repo.InsertInvalidDates(invalidDates);
            });

            t.Wait();
            var response = t.Result;
            if (!response.IsSuccessful) throw response.Error;
        }
        #endregion

    }
}
